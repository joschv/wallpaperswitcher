﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace WallpaperByDaytime
{
    class Program
    {
        [DllImport("user32.dll")]
        public static extern Int32 SystemParametersInfo(UInt32 action, UInt32 uParam, String vParam, UInt32 winIni);

        public static readonly UInt32 SPI_SETDESKWALLPAPER = 0x14;
        public static readonly UInt32 SPIF_UPDATEINIFILE = 0x01;
        public static readonly UInt32 SPIF_SENDWININICHANGE = 0x02;

        // from 0 until first entry first wallpaper
        // from last entry until 24 last wallpaper
        // x entries means x+1 different wallpapers
        public static readonly int[] time_list = { 
            9,
            16
        };
        // wallpaper file names
        public static readonly string[] wallpaper_list =
        {
            "wallpaper-1.jpg",  // in given example 0 - 8
            "wallpaper-2.jpg",  // 9 - 15
            "wallpaper-3.jpg"   // 16 - 24
        };
        // how many seconds until refresh
        public static readonly int refreshInterval = 120;

        public static void SetWallpaper(String path)
        {
            Console.WriteLine("Setting wallpaper to '" + path + "'");
            SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, path, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
        }



        public static void Main(string[] args)
        {
            string thisDir = AppDomain.CurrentDomain.BaseDirectory;
            Console.WriteLine("Searching for wallpapers in: " + thisDir);
            bool running = true;
            int last = -1;
            while (running)
            {
                // get current time
                DateTime time = DateTime.Now;
                int hour = time.Hour;
                int minute = time.Minute;
                // check time slots
                int i = 0;
                while(hour >= time_list[i] && i < time_list.Length)
                {
                    i++;
                }
                // set wallpaper
                if (i != last) SetWallpaper(thisDir + wallpaper_list[i]);
                last = i;
                // wait until refresh
                System.Threading.Thread.Sleep(refreshInterval * 1000);
            }
        }
    }
}
